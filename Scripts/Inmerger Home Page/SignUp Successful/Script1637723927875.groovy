import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Comon/Navigate site staging successful'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Inmerger Home Page/SignUp/btn_signUp_onMenuBar'))

WebUI.setText(findTestObject('Inmerger Home Page/Login/tbx_Email'), 'theanhtest002@gmail.com')

WebUI.setEncryptedText(findTestObject('Inmerger Home Page/Login/tbx_password'), 'HeCM15nHKBI=')

WebUI.click(findTestObject('Inmerger Home Page/SignUp/chk_accecptAgreement'))

WebUI.click(findTestObject('Inmerger Home Page/SignUp/btn_signUp_onPopUp'))

WebUI.waitForElementVisible(findTestObject('Inmerger Home Page/SignUp/lbl_signUpSuccessful'), GlobalVariable.G_timeOut)

actualUrl = WebUI.getUrl()

WebUI.verifyMatch(actualUrl, 'https://inmerger-client.impetus.vn/register/complete', true)

WebUI.closeBrowser()

